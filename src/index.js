import React from 'react';
import ReactDOM from 'react-dom';
// Styles
import './theme.scss';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './main.scss';


import { Provider } from 'react-redux';
import { store } from './helpers';

import * as serviceWorker from './serviceWorker';
import App from './components/App';

ReactDOM.render(
<Provider store={store}>
    <App />
</Provider>, document.getElementById('root'));


serviceWorker.unregister();
