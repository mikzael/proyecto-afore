import axios from 'axios'
import { propiedades } from './propiedades.js'

const configuracionHeader = {
    headers: {
        Authorization: "Bearer " + sessionStorage.getItem("tokenjwt")
    }
}

const configuracionHeaderJSON = {
    headers: {
        'Content-Type': 'application/json',
        Authorization: "Bearer " + sessionStorage.getItem("tokenjwt")
    }
}

class CatalogoService {

    constructor(props) {        
        this.config="";
    }

    logout() {
        sessionStorage.removeItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME);
        window.location.href = "/main"
    }

    obtenerAdministrador(idAdministrador) {        
      return axios.get(`${propiedades.SERVER_URL}/usuario-rest/getUserById/` + idAdministrador, configuracionHeader);
    }

    guardarNuevoAdministrador(usuarioIdentity) {                
        return axios.post(`${propiedades.SERVER_URL}/usuario-rest/administrar`, usuarioIdentity, configuracionHeaderJSON);
    }

    guardarAdministrador(usuarioIdentity) {                
        return axios.put(`${propiedades.SERVER_URL}/usuario-rest/administrar`, usuarioIdentity, configuracionHeaderJSON);
    }
	
    borrarAdministrador(usuarioIdentity) {        
        return axios.post(`${propiedades.SERVER_URL}/usuario-rest/administrar/borrar`, usuarioIdentity, configuracionHeaderJSON);
    }

    modificarPassword(usuarioIdentityPassword) {
        return axios.post(`${propiedades.SERVER_URL}/usuario-rest/password/modificar`, usuarioIdentityPassword, configuracionHeaderJSON);
    }

    obtenerUsuariosIdentity() {        
        return axios.get(`${propiedades.SERVER_URL}/usuario-rest/all`, configuracionHeader);
    }

    obtenerUsuariosIdentityById(id) {        
        return axios.get(`${propiedades.SERVER_URL}/usuario-rest/getUserById/${id}`, configuracionHeader);
    }

}

export default new CatalogoService()