export const propiedades = {
    SERVER_URL: 'http://localhost:7020/identity-manager',
	OAUTH_SERVER_URL:'http://localhost:/identity/autorizacion/oauth/token',
    USER_NAME_SESSION_ATTRIBUTE_NAME: 'authenticatedUser',
    USER_NAME_SESSION_ATTRIBUTE_ROLE: 'authenticatedUserRole',
    CRYPTO_SECRET : 'arquitectura-reactjs-secret'
};