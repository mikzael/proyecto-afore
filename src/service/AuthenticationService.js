import axios from 'axios'
import { propiedades } from './propiedades.js'

const configuracionHeader = {
    headers: {
        Authorization: "Bearer " + sessionStorage.getItem("tokenjwt")
    }
}

class AuthenticationService {

    constructor(props) {        
        this.config="";
    }

    executeBasicAuthenticationService(username, password) {
        return axios.get(`${propiedades.SERVER_URL}/basicauth`,
            { headers: { authorization: this.createBasicAuthToken(username, password) } })
    }

    executeJwtAuthenticationService(username, password) {
        console.log(username);
        return axios.post(`${propiedades.SERVER_URL}/authenticate`, {
            username,
            password
        })
    }

    createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    registerSuccessfulLogin(username, password) {
        sessionStorage.setItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        sessionStorage.setItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_ROLE, "ADMINISTRADOR")
        this.setupAxiosInterceptors(this.createBasicAuthToken(username, password))
    }

    registerSuccessfulLoginForJwt(username, token) {
        sessionStorage.setItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        sessionStorage.setItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_ROLE, "ADMINISTRADOR")
        sessionStorage.setItem("tokenjwt", token)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        console.log("creando token")
        return 'Bearer ' + token
    }

    logout() {
        sessionStorage.removeItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME);
        window.location.href = "/arquitectura/login"
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) 
            //return false
            return true
        return true
    }

    getUserLoggedRole() {
        let userRole = sessionStorage.getItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_ROLE)
        console.log(userRole);
        return userRole;
    }

    getLoggedInUserName() {
        let user = sessionStorage.getItem(propiedades.USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return ''
        return user
    }    

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                console.log("axios interceptor")
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }

    
}
export default new AuthenticationService()