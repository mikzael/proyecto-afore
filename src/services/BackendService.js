/* Service Manager
Global service consumption generic and parameterizable.
Services
*/
// Imports
import axios from "axios";
import core from "../helpers/core";
// Services
import AuthenticationService from "./AuthenticationService";

// Axios config
const config = {
    baseURL: process.env.REACT_APP_URL,
    timeout: 10000, // 10 Seconds
    params: {
        api_key: process.env.REACT_APP_API_KEY
    },
    headers: {
        ['Content-Type']: 'application/json',
        ['Access-Control-Allow-Origin']: '*'
    }
}

class BackendService {
    fetch(url, body) {
        let axiosRequest = axios.post(url, body, config)
        // Internal promise axios catch;
        axiosRequest.then((response) => {
            // handle success
            console.log(response);
            console.log(core.checkIfResponseValids(response))
        })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                console.log("always executes");
                // always executed
            });
        return axiosRequest;
    }
}

export default new BackendService()