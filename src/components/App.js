import React from 'react';
// Components
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import AuthenticatedRoute from './auth/AuthenticatedRoute';
import Header from './header/Header';
import Register from './register/Register';
import Login from './login/Login';
import RegistroMenores from './registroMenores/registroMenores';
import HomeComponent from './home/HomeComponent';
import FooterComponent from './footer/FooterComponent';

const App = () => {
    return (
        <React.StrictMode>
            <Router>
                <div id="mainAppDiv">
                    <Header />
                    <Switch>
                        <Route path="/" exact component={HomeComponent} />
                        <Route path="/arquitectura" exact component={HomeComponent} />
                        <Route path="/login" component={Login} />
                        <Route path="/menores" exact component={RegistroMenores} />
                        <Route path="/registro" exact component={Register} />
                        <AuthenticatedRoute path="/arquitectura/home" exact component={HomeComponent} />
                        <Route path='*'>
                            <Redirect to='/' />
                        </Route>
                    </Switch>
                    <FooterComponent />
                </div>
            </Router>
        </React.StrictMode>
    )
}

export default App;
