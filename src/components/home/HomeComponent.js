import React, { Component, useState } from "react";
import { Button } from "./../misc/button/Button";
import { ModalDownload } from "../misc/modal";
import { FindYourAforeComponent } from "./";
import aforewebImg from "../../assets/img/header/logo-afore.png";
import ImagenAppleStore from "../../assets/img/ImagenAppleStore.png";
import ImagenGooglePlay from "../../assets/img/ImagenGooglePlay.png";
import ImagenHuawei from "../../assets/img/ImagenHuawei.png";
import ImagenModalConsar from "../../assets/img/ImagenModalConsar.png";
import ImagenCode from "../../assets/img/ImagenCode.png";

import "./home.scss";

const HomeComponent = props => {
  const [aforeTypes, setAforeTypes] = useState(true);
  const [view, setView] = useState(0);
  const [openDialogCompleted, setOpenDialogCompleted] = useState(false);

  // Dialog path completed
  const handleOpenDialogCompleted = () => {
    setOpenDialogCompleted(true);
  };

  const handleCloseDialogCompleted = () => {
    setOpenDialogCompleted(false);
  };
  return (
    <div>
      {view === 1 ? (
        <FindYourAforeComponent />
      ) : (
        <section className="sectionContainer">
          {aforeTypes ? (
            <div style={{ display: "flex" }}>
              {" "}
              <div style={{ paddingTop: "28px", color: "#62b5e5" }}>
                Estás afiliado en:
              </div>{" "}
              <div className="imageTypesAfore" />{" "}
            </div>
          ) : (
            <div className="aforeName">Estás registrado en (NOMBRE AFORE)</div>
          )}
          <div className="boxContent">
            <div className="primaryContent">
              <div className="imageOne">
                <div className="contentOpacity">
                  <div style={{ height: "100%" }}>
                    <p className="tittleContentOne">
                      Construye <br /> el futuro que deseas hoy
                    </p>
                    <p className="tittleContentTwo">
                      Registrate en una AFORE y comienza a ahorrar para tu
                      retiro
                    </p>
                    <div style={{ paddingTop: "20px" }}>
                      <Button className="p-button-secondary buttoncl">
                        Entérate cómo
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="contentTwo">
              <div className="itemAfore">
                <div className="imageAforeType">
                  <div
                    style={{
                      color: "#ffffff",
                      //textAlign: "center",
                      fontSize: "19px",
                      paddingLeft: "10px",
                      paddingTop: "17px"
                    }}
                  >
                    Haz crecer tu cuenta
                  </div>
                  <div
                    style={{
                      color: "#ffffff",
                      //textAlign: "center",
                      fontSize: "13px",
                      paddingLeft: "10px",
                      paddingTop: "17px"
                    }}
                  >
                    Ahorrar voluntariamente en tu AFORE, tiene importantes
                    beneficios.
                  </div>
                  <div
                    style={{
                      paddingTop: "20px",
                      float: "right",
                      paddingRight: "27px"
                    }}
                  >
                    <Button className="p-button-secondary">
                      Enterate cómo
                    </Button>
                  </div>
                </div>
                {/*<div className="imageTwo"></div>*/}
              </div>
              <div className="calculatorContent">
                <div className="imageKid">
                  <div
                    style={{
                      color: "#ffffff",
                      //textAlign: "center",
                      fontSize: "19px",
                      paddingLeft: "10px",
                      paddingTop: "17px"
                    }}
                  >
                    Calculadora{" "}
                  </div>
                  <p
                    style={{
                      color: "#ffffff",
                      //textAlign: "center",
                      fontSize: "14px",
                      paddingLeft: "10px",

                      paddingTop: "23px"
                    }}
                  >
                    Estima cuál podría ser tu ahorro en el futuro.
                  </p>
                  <Button
                    style={{ marginTop: "37px", marginLeft: "20px" }}
                    className="p-button-secondary"
                  >
                    Ingresa Aqui
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="banner">
            <div className="contentTitleBanner">
              <img
                src={aforewebImg}
                alt="aforeWeb"
                height="100px"
                className="p-ml-6"
              />
            </div>
            <div className="contentTwoBanner">
              <div className="appDesc">
                Descarga AforeMóvil desde tu tienda y obtén todos los beneficios
                de tu AFORE al alcance de tu mano.
              </div>
            </div>
            <div className="contentThreeBanner">
              <Button
                onClick={handleOpenDialogCompleted}
                className="p-button-secondary"
              >
                Descarga la APP
              </Button>
            </div>
            <div className="contentImageBanner">
              <div className="imageTelBanner"></div>
            </div>
          </div>

          <ModalDownload
            action={handleCloseDialogCompleted}
            header={"¡Felicidades!"}
            visible={openDialogCompleted}
            canClose={false}
            actionBtn="Aceptar"
          >
            <div className="imageDialogDownload"></div>
            <div
              style={{
                fontSize: "13px",
                fontWeight: 100,
                textAlign: "justify",
                paddingTop: "20px",
                paddingBottom: "20px"
              }}
            >
              Afore Móvil es una Aplicación para teléfonos inteligentes que te
              permite llevar el control del ahorro en tu Cuenta AFORE en todo
              momento, estés donde estés. Descargala desde tu tienda virtual:
            </div>
            <div className="flexcontainerDownload">
              <div className="itemsDownload">
                <div
                  style={{
                    backgroundImage: `url(${ImagenAppleStore})`
                  }}
                  className="centerImageDownload"
                ></div>
              </div>
              <div className="itemsDownload">
                <div
                  style={{
                    backgroundImage: `url(${ImagenGooglePlay})`
                  }}
                  className="centerImageDownloadGoogle"
                ></div>
              </div>
              <div className="itemsDownload">
                <div
                  style={{
                    backgroundImage: `url(${ImagenHuawei})`
                  }}
                  className="centerImageDownload"
                ></div>
              </div>
            </div>
            <div className="flexcontainerDownload">
              <div className="itemsDownloadText">
                Si tu Celular es IOS, <a href="url">descargala aqui</a>
              </div>
              <div className="itemsDownloadText">
                Si tu Celular es Android, <a href="url">descargala aqui</a>
              </div>
              <div className="itemsDownloadText">
                Si tu Celular es Huawei, <a href="url">descargala aqui</a>
              </div>
            </div>
            <div className="flexcontainerDownload">
              <div className="itemsDownload">
                {" "}
                <div
                  style={{
                    backgroundImage: `url(${ImagenCode})`
                  }}
                  className="centerImageDownload"
                ></div>
              </div>
              <div className="itemsDownload">
                {" "}
                <div
                  style={{
                    backgroundImage: `url(${ImagenCode})`
                  }}
                  className="centerImageDownload"
                ></div>
              </div>
              <div className="itemsDownload">
                {" "}
                <div
                  style={{
                    backgroundImage: `url(${ImagenCode})`
                  }}
                  className="centerImageDownload"
                ></div>
              </div>
            </div>
            <p className="tittleFooter">
              ¡Así de fácil es crear el futuro que quieras!
            </p>
          </ModalDownload>
        </section>
      )}
    </div>
  );
};
export default HomeComponent;
