import React, { Component, useState } from "react";
import { Button } from "./../misc/button/Button";
import { ModalDownload } from "../misc/modal";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";

import "./findafore.scss";

const FindYourAforeComponent = props => {
  return (
    <section className="sectionContainer p-find-afore-container ">
      <div className="title">
        <div className="p-text-inicio">Inicio \ </div>
        <div className="p-second-text-inicio"> Localiza tu AFORE</div>
      </div>
      <div>¿No sabes en qué AFORE estás?</div>
      <div className="text-tittle">
        Bienvenido al servicio Localiza tu AFORE, donde podrás consultar la
        AFORE en la que estás registrado.
      </div>
      <div className="text-tittle">
        Para realizar este trámite es necesario:
      </div>
      <div className="text-tittle-bold">
        1. Número de Seguridad Social (NSS) o Clave Única de Registro de
        Población (CURP).
      </div>
      <div className="text-tittle-bold">
        2. Cuenta de correo a la que deseas que se te envíe la información.
      </div>
      <div className="text-tittle">
        Te recordamos que es muy importante que tu cuenta esté registrada en una
        AFORE, para que recibas los beneficios como el estado de cuenta tres
        veces al año a tu domicilio y llevar el control de tu ahorro, así como
        ganar atractivos rendimientos por la inversión de tus recursos
      </div>
      <div className="text-tittle-bold">
        Para iniciar tu trámite llena el siguiente formulario:
      </div>
      <div className="text-tittle-min">Selecciona una opción</div>
      <div className="formInputs p-d-flex p-flex-column p-mb-6">
        <span className="p-float-label  p-mb-4">
          <Dropdown
            id="colonia"
            style={{
              borderRadius: "10px",
              width: "500px"
            }}
          />
          <label htmlFor="colonia">Número de Seguridad Social (NSS)*</label>
        </span>
        <span className="p-float-label  p-mb-4">
          <InputText
            style={{
              borderRadius: "10px",
              width: "500px"
            }}
            id="mail"
          />
          <label htmlFor="mail">NSS*</label>
        </span>
        <span className="p-float-label  p-mb-4">
          <InputText
            style={{
              borderRadius: "10px",
              width: "500px"
            }}
            id="mail"
          />
          <label htmlFor="mail">Correo electrónico*</label>
        </span>
        <div
          style={{ textAlign: "right", width: "500px" }}
          className="text-tittle-min"
        >
          Obligatorio*
        </div>
      </div>
      <div>
        <Button
          style={{ marginLeft: "50%", marginRight: "50" }}
          className="p-button-secondary"
        >
          Descarga la APP
        </Button>
      </div>
      <p className="tittleFooter-afore">
        AVISO: Solo podrás realizar una consulta al día, que será enviada a la
        cuenta de correo que proporciones.
      </p>
    </section>
  );
};
export default FindYourAforeComponent;
