import React from 'react';
// Media and styles
import './LateralBanner.scss';

const LateralBanner = () => {
    return (
        <div className="lateralBanner p-ml-6 p-d-f p-flex-colum">
            <div className="lateralButton p-mb-4"></div>
            <div className="lateralButton p-mb-4"></div>
            <div className="lateralButton"></div>
        </div>
    )
}
export {LateralBanner}