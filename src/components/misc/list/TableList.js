import React from 'react'
import './TableList.scss';

/*
    Este componente muestra una lista con forma de tabla
*/

const TableList = ({ values, striped = true }) => {
    return (
        <ul className={striped && 'striped'}>
            {values.map((entry, i) => (
                <li key={i}>
                    <div className="p-grid p-m-0">
                        <div className="p-col p-ml-6 text-key">{entry.key}</div>
                        <div className="p-col text-value">{entry.value}</div>
                    </div>
                </li>
            ))}
        </ul>
    )
}

export default TableList
