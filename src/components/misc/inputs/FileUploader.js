import React from 'react';

import { Button } from 'primereact/button';

const FileUploader = props => {
    const hiddenFileInput = React.useRef(null);

    const handleClick = event => {
        hiddenFileInput.current.click();
    };
    const handleChange = event => {
        props.handleFile(event);
    };
    return (
        <>
            <Button label={props.label} onClick={handleClick} className={props.classComponent} />

            <input type="file"
                   ref={hiddenFileInput}
                   onChange={handleChange}
                   style={{display:'none'}}
            />
        </>
    );
};
export default FileUploader;
