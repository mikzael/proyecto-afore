import React from 'react';
import { Button as Btn } from 'primereact/button';
// Media and styles
import './Button.scss';
import Arrow from '../../../assets/img/arrow.svg';

const Button = (props) => {
    return (
        <Btn {...props}
            className={`buttonAfore ${props.className}`}>
            <img src={Arrow} className="arrow" />
            {props.children}
            <div className="extra"></div>
        </Btn>
    )
}
export {Button};