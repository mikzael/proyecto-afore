// Made by Antony Morlaes
import React, { useState, useEffect } from 'react';
import { Dialog } from 'primereact/dialog';
//import { Button } from 'primereact/button';
import { Button } from '../../misc/button/Button';
// Media and styles
import './Modal.scss';

const Modal = (props) => {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const renderFooter = (onHide, action, canClose, actionBtn, cancelBtn) => {
        return (
            <div className="p-d-flex p-jc-center">
                {canClose &&
                    <Button label={cancelBtn ? cancelBtn : "Cancelar"} onClick={onHide} className="p-button-secondary p-mr-5" />
                }
                <Button label={actionBtn ? actionBtn : "Continuar"} onClick={action} autoFocus />
            </div>
        );
    }

    return (
        <Dialog className={`dialogAfore strong ${!props.canClose ? "hideClose" : ""}`} header={props.header} visible={visible} footer={() => renderFooter(props.onHide, props.action, props.canClose, props.actionBtn, props.cancelBtn)} onHide={props.onHide} draggable={false} resizable={false}>
            {props.children}
        </Dialog>
    )
}

export { Modal };