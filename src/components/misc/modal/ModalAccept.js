// Made by Antony Morlaes
import React, { useState, useEffect } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from '../../misc/button/Button';
import { Checkbox } from 'primereact/checkbox';
// Media and styles
import './Modal.scss';

const ModalAccept = (props) => {
    const [visible, setVisible] = useState(false);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const renderFooter = (action, actionBtn, acceptBtn) => {
        return (
            <div className="p-d-flex p-jc-between p-ai-center">
                <div className="p-field-checkbox">
                    <Checkbox inputId="binary" checked={checked} onChange={e => setChecked(e.checked)} />
                    <label htmlFor="binary">{acceptBtn ? acceptBtn : "Aceptar"}</label>
                </div>
                <Button label={actionBtn ? actionBtn : "Continuar"} onClick={action} autoFocus disabled={!checked}/>
            </div>
        );
    }

    return (
        <Dialog className={`dialogAfore ${!props.canClose ? "hideClose" : ""}`} header={props.header} visible={visible} footer={() => renderFooter(props.action, props.actionBtn, props.acceptBtn)} onHide={props.onHide} draggable={false} resizable={false} >
            {props.children}
        </Dialog>
    )
}

export { ModalAccept };