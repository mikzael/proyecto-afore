import React, { useRef } from 'react';
import './Stepper.scss'

let onSelect = null;

function growingSteps( i, activeIndex) {
    if (i < activeIndex - 1) {
        return(
            <li className="active"  onClick={() => onSelect(i+1)}>
                <div className="text-style filled">
                    <span>{i + 1}</span>
                </div>
            </li>
        )
    }

    if (i === activeIndex - 1) {
        return(
            <li className="current" onClick={() => onSelect(i+1)}>
                <div className="text-style filled">
                    <span>{i + 1}</span>
                </div>
            </li>
        )
    }

    if (i > activeIndex - 1) {
        return(
            <li className="next"  onClick={() => onSelect(i+1)}>
                <div className="text-style">
                    <span>{i + 1}</span>
                </div>
            </li>
        )
    }
}

function Steps({ numberSteps, activeIndex }) {
    let renderSteps = [];

    if (numberSteps) {
        if (numberSteps > 2) {
            for (let i = 0; i < numberSteps; i++) {
                renderSteps.push(
                    growingSteps(i,activeIndex)
                )
            }
        } else {
            for (let i = 0; i < 2; i++) {
                renderSteps.push(
                    growingSteps(i,activeIndex)
                )
            }
        }
    }

    return renderSteps;
}

function Seteper(props) {

    const progressBar = useRef(null)

    let { model, activeIndex } = props;
    let numberSteps = 0;

    if (!activeIndex)
        activeIndex = 1


    if (model) {
        numberSteps = Array.isArray(model) ? model?.length : model;
        if(numberSteps < 2)
            numberSteps = 2;

        if (activeIndex < 1)
            activeIndex = 1
        if (activeIndex > numberSteps)
            activeIndex = numberSteps
    }

    else
        numberSteps = 2;

    if(props.onSelect)
        onSelect = props.onSelect;

    React.useEffect(() => {
        progressBar.current.style.setProperty('--overlaySteps', numberSteps)
    })
    return (
        <div className="wrapper-progressBar">
            <ul className="progressBar" ref={progressBar}>
                <Steps numberSteps={numberSteps} activeIndex={activeIndex}/>
            </ul>
        </div>
    )
}


export default Seteper;