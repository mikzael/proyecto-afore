import React, { useState, useEffect } from 'react';
import { Steps } from 'primereact/steps';
import ValCurp from './valCurpComponent';
import ServiceContract from './serviceContract';
import ComplementaryData from './ComplementaryData';
import KidsList from './KidsList';
import BoxShadow from './BoxShadow';
import { ModalAccept } from '../misc/modal/ModalAccept';
import TableList from '../misc/list/TableList';

import { LateralBanner } from '../misc/lateralBanner/LateralBanner';

//import { Button } from 'primereact/button';
import { Avatar } from 'primereact/avatar';
import { InputText } from 'primereact/inputtext';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Container from './Container';

import { ScrollPanel } from 'primereact/scrollpanel';

import Stepper from '../misc/Stepper/Stepper';
import { Button } from '../misc/button/Button';

//Images
import childrenbanner from './../../assets/img/children-registry-banner.svg'
import nacimiento from './../../assets/img/nacimiento.svg'

//styles
import './registroMenores.scss'


function RegistroMenores() {

    const [activeIndex, setActiveIndex] = useState(4);
    const [saldo, setSaldo] = useState(0); // Borrar
    const [openModalPrivacy, setOpenModalPrivacy] = useState(false);

    const items = [
        { label: '' },
        { label: '' },
        { label: '' },
    ];

    useEffect(() => {
        document.getElementsByClassName('kids-title')[0].scrollIntoView()

        if(activeIndex === 5){ // asignamos variable
            let random = Math.floor(Math.random() * 3) + 1 ;
            setSaldo(random);
        }

    }, [activeIndex])

    const responseCurp = [ // Se emula la respuesta del servicio que valida el curp.
        { key: "CURP:", value: "CAGL860927HDFSTS02" },
        { key: "Nombre(s):", value: "José Luis" },
        { key: "Apellido Paterno:", value: "Castillo" },
        { key: "Apellido Materno:", value: "Gutiérrez" },
        { key: "Nacionalidad:", value: "Mexicana" },
        { key: "Estado:", value: "Ciudad de México" },
        { key: "Género:", value: "Masculino" },
        { key: "Fecha de nacimiento:", value: "20 de junio de 2012" },
    ];

    // Se emula la respuesta del servicio que regresa la info del menor registrado
    const responseDetailKidRegister = responseCurp.unshift({key: "Alias:", value: "Majo"});

    return (
        <section className="kids-registry p-mr-5">

            <div className="kids-registry-container">

                <div style={{ width: 500, marginLeft: '25%' }}>
                    {/* <Steps model={items} activeIndex={activeIndex} onSelect={(e) => setActiveIndex(e.index)} readOnly={false} /> */}
                    {
                        activeIndex === 0 &&
                        <Container
                            title="Registro de menores"
                            >
                            <img src={childrenbanner} width="180%" alt="Bienvenida registro menores" className="p-mt-7 p-mb-6" />
                            <ValCurp setActiveIndex={setActiveIndex} />
                        </Container>
                    }
                    {activeIndex === 1 &&
                        <div className="kids-parent-container">
                            <Stepper model={3} activeIndex={activeIndex} onSelect={(index) => setActiveIndex(index)} />
                            <Container
                                title="Confirmación de datos"
                                subtitle={
                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />Suspendisse mollis fringilla dolor vitae feugiat.</span>
                                }
                            >
                                <TableList values={responseCurp} />
                                <Button className="p-mt-5" onClick={() => setActiveIndex(2)}>Continuar</Button>
                            </Container>
                        </div>
                    }
                    {activeIndex === 2 &&
                        <div className="kids-parent-container">
                            <Stepper model={3} activeIndex={activeIndex} onSelect={(index) => setActiveIndex(index)} />
                            <Container
                                title="Datos complementarios"
                                subtitle={
                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />Suspendisse mollis fringilla dolor vitae feugiat.</span>
                                }
                            >
                                <ComplementaryData />
                                <div className="text-center p-mt-3 color-grey"> Adjunta el acta de nacimiento del menor
                                </div>
                                <div className="p-grid p-mt-2" style={{alignItems: 'center'}}>
                                    <div className="p-col-2">
                                        <img height="60px" src={nacimiento} />
                                    </div>
                                    <div className="p-col-8">
                                        <div className="p-fluid p-formgrid p-grid texto-derecha ">
                                            <div className="p-field p-col p-mt-2">
                                                <InputText />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="p-col-2">
                                        <input type='file' id='file' style={{display: 'none'}}/>
                                        <Button onClick={() => document.getElementById('file').click()}>Buscar</Button>
                                    </div>
                                </div>
                                <Button className="p-mt-5" onClick={() => setOpenModalPrivacy(true)}>Continuar</Button>
                            </Container>
                        </div>
                    }
                    {activeIndex === 3 &&
                        <div className="kids-parent-container">
                            <Stepper model={3} activeIndex={activeIndex} onSelect={(index) => setActiveIndex(index)} />
                            <Container
                                title="Contrato de servicio"
                                subtitle={
                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />Suspendisse mollis fringilla dolor vitae feugiat.</span>
                                }
                            >
                                <ServiceContract setActiveIndex={setActiveIndex}/>
                            </Container>
                        </div>
                    }
                    {activeIndex === 4 &&
                        <div className="kids-parent-container">
                            <Container
                                title="Menores registrados"
                            >
                                <KidsList setActiveIndex={setActiveIndex} />
                                <Button className="p-mt-5" onClick={() => setActiveIndex(0)}>Registra menor de edad</Button>
                            </Container>
                        </div>
                    }
                    {activeIndex === 5 &&
                        <div className="kids-parent-container">
                            <Container
                                title="Detalle del menor registrado"
                            >
                                <div className="p-mb-4 w-100">
                                    <TableList values={responseCurp} />
                                </div>
                                {saldo === 1 &&
                                    <BoxShadow>
                                        <div className="color-grey p-pt-2 text-center">
                                            <h3>El ahorrador no cuenta con saldo en su cuenta</h3>
                                        </div>
                                    </BoxShadow>
                                }
                                {saldo === 2 &&
                                    <BoxShadow>
                                        <div className="color-grey p-pt-2 text-center">
                                            <h3>Servicio de la Administradora no disponible para <br /> consulta de saldos</h3>
                                        </div>
                                    </BoxShadow>
                                }
                                {saldo === 3 &&
                                    <BoxShadow>
                                        <div className="color-grey p-pt-2 text-center">
                                            <h3 className="color-green">Aportaciones voluntarias</h3>
                                            <h3 className="color-blue">$ 1,000.00</h3>
                                        </div>
                                    </BoxShadow>
                                }

                                <Button className="p-button-secondary p-mt-5" onClick={() => setActiveIndex(4)}>Regresar</Button>
                            </Container>
                        </div>
                    }
                </div>
            </div>
            <LateralBanner></LateralBanner>
            <ModalAccept
                header={"Aviso de privacidad Afore Web"}
                visible={openModalPrivacy}
                onHide={() => setOpenModalPrivacy(false)}
                action={() => {
                    setOpenModalPrivacy(false)
                    setActiveIndex(3)
                }}
                acceptBtn="He leído y acepto el Aviso de Privacidad."
            >
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan-dit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
            </ModalAccept>
        </section>
    )
}

export default RegistroMenores;
