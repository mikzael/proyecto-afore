import React from 'react';
import KidsCard from './KidsCard';
import './registroMenores.scss';

const KidsList = (props) => {
    return (
        <div className="p-grid p-dir-col">
            {[1, 2, 3, 4, 5].map((kid, i) => (
                <KidsCard key={i} kid={kid} setActiveIndex={props.setActiveIndex} />
            ))}
        </div>
    )
}

export default KidsList;
