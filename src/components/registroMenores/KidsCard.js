import React from 'react';
import profuturo from "../../assets/img/profuturo.svg";
import sura from "../../assets/img/sura.svg";
import BoxShadow from './BoxShadow';
import './registroMenores.scss';
import { ReactComponent as BoyIcon } from '../../assets/img/boy.svg';
import { ReactComponent as GirlIcon } from '../../assets/img/girl.svg';



const KidsCard = ({ kid, setActiveIndex }) => {

    return (
        <a href={void(0)} onClick={() => setActiveIndex(5)} className="card-kids">
            <BoxShadow>
                <div className="p-col">
                    <div className="p-grid">
                        <div className="p-col-2 img-kid">
                            { kid % 2 ? <BoyIcon /> : <GirlIcon /> }
                        </div>
                        <div className="p-col-6">
                            <ul>
                                <li>
                                    <span className="color-green">Alias: </span>
                                    <span className="color-blue">Nombre {kid}</span>
                                </li>
                                <li>
                                    <span className="color-green">CURP: </span>
                                    <span className="color-blue">XAXA877665RTE</span>
                                </li>
                            </ul>
                        </div>
                        <div className="p-col-4">
                            <img
                                src={kid % 2 ? profuturo : sura}
                                alt="Logotipo aseguradora"
                                className="img-brand"
                            />
                        </div>
                    </div>
                </div>
            </BoxShadow>
        </a>
    )
}

export default KidsCard;
