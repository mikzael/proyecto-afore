import React, {useState, useEffect} from 'react'
import { InputText } from 'primereact/inputtext';
import { Button } from '../misc/button/Button';
import { Modal } from '../misc/modal/Modal';
//styles
import './registroMenores.scss'

function regexCURP(curp){
    const regex = new RegExp("[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}" +
            "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
            "[HM]{1}" +
            "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
            "[B-DF-HJ-NP-TV-Z]{3}" +
        "[0-9A-Z]{1}[0-9]{1}$");

    if(regex.test(curp))
        return true;
    else 
        return false;
}

function useActionsModal(){
    const [openDialogCurp, setOpenDialogCurp] = useState(false);

    const switchDialogCurp = () => {
        setOpenDialogCurp(prevOpenDialogCurp => !prevOpenDialogCurp);
    }
    return { openDialogCurp, switchDialogCurp}
}

function useActionsInputCurp(){
    const [curp, setCurp] = useState('');
    const [isDisabledButton, setDisabledButton] = useState(true);

    useEffect(() => {
        //Validación de Curp
        if (curp.length >= 18){
            if(regexCURP(curp))
                setDisabledButton(false);
        } else {
            setDisabledButton(true)
        }
    },[curp])

    const handleChangeCurp = e => {
        let val = e.target.value;
        setCurp(String(val.toUpperCase()));
    }

    return { 
        curp, 
        isDisabledButton,
        handleChangeCurp
    };
}

const ValCurp = (props) => {

    const { setActiveIndex } = props;

    const {openDialogCurp, switchDialogCurp} = useActionsModal();
    const {
        curp,
        isDisabledButton,
        handleChangeCurp 
    } = useActionsInputCurp();

    const handleEnter = e => {
        if(e.key === 'Enter' && isDisabledButton === false){
            switchDialogCurp();
        }
    }

    const handleErrorAdvice = () => {
        return curp !=="" && isDisabledButton === true ? true : false;
    }
    

    return (
        <div>
            <div className="kids-form p-d-flex p-flex-column p-jc-center p-ai-center">
                <div className="p-mt-4">
                    <span className="kids-subtitle">Ingresa la CURP de tu hijo(a) que deseas registrar,</span>
                </div>
                <div className="kids-margin-bottom">
                    <span className="kids-subtitle">recuerda que tiene que ser menor de 18 años.</span>
                </div>

                <div className="formInputs p-d-flex p-ai-center ">
                    <div className="p-field">
                    <span className="p-float-label p-mb-0">
                        <InputText id="kid-curp" className={(handleErrorAdvice() && "p-invalid") + " p-inputtext-sm"} value={curp} onChange={e => handleChangeCurp(e)} onKeyDown={e => handleEnter(e)}/>
                        <label htmlFor="curp" className={(handleErrorAdvice() ? "p-invalid": "")}>CURP*</label>
                    </span>
                    {
                        handleErrorAdvice() && <small id="username2-help" className="p-error p-d-block">La CURP que ingresaste es incorrecta, verifícala e intenta nuevamente.</small>
                    }
                    </div>
                </div>
                <div className="kids-obligatorio">
                    <label htmlFor="obligatorio">*Obligatorio</label>
                </div>

                <div className="p-mt-4">
                    <span className="kids-subtitle">¿Tienes dudas de la CURP?,</span>
                </div>
                <div className="p-mb-4">
                    <span className="kids-subtitle"><a href="">¡Da clic aquí</a> y te llevaremos a RENAPO para ayudarte!</span>
                </div>

                <Button className="p-mt-5" onClick={switchDialogCurp} disabled={isDisabledButton}>Continuar</Button>
            </div>

            <Modal header={"Aviso"} visible={openDialogCurp} onHide={switchDialogCurp} action={() => setActiveIndex(1)} canClose={false} actionBtn="Continuar">
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit accumsan, eleifend odio ullamcorper metus turpis venenatis egestas dignissim magna, iaculis nulla non montes felis habitant vulputate. Fames cras conubia dignissim tempus duis, vestibulum hendrerit tempor libero justo inceptos, nec diam ornare vehicula.</p>
            </Modal>
        </div>
    )
}

export default ValCurp;
