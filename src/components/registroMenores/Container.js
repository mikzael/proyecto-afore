import React from 'react';
import './registroMenores.scss'
import { Button } from 'primereact/button';
import { Avatar } from 'primereact/avatar';
import { InputText } from 'primereact/inputtext';


const Container = (props) => {

    const { title, subtitle, children } = props;

    return (
        <div>
            <div className="kids-form p-d-flex p-flex-column p-jc-center p-ai-center">
                <h1 className="kids-title">{title}</h1>
                <div className="kids-header">
                    <div className="kids-margin-top kids-margin-bottom">
                        <span className="kids-subtitle">{subtitle}</span>
                    </div>
                </div>
                {children}
            </div>
        </div>
    )
}

export default Container;
