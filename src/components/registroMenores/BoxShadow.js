import React from 'react';

const BoxShadow = (props) => {

    const { children } = props;

    return (
        <div className="box-shadow">
            {children}
        </div>
    )
}

export default BoxShadow;
