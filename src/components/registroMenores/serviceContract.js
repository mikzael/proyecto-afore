import {useState} from  'react';
import { InputText } from 'primereact/inputtext';
import { ScrollPanel } from 'primereact/scrollpanel';
import { Button } from '../misc/button/Button';
import { Modal } from '../misc/modal/Modal';

function useActionsModal(){
    const [openDialog, setOpenDialog] = useState(false);

    const switchDialog = () => {
        setOpenDialog(prevOpenDialog => !prevOpenDialog);
    }
    return { openDialog, switchDialog}
}

function useTogglePasswordVisibiliy(){
    const [isVisible, setIsVisible] = useState(false);

    const togglePasswordVisibility = () => {
        setIsVisible(prevIsVisible =>  !prevIsVisible)
    }

    return {isVisible, togglePasswordVisibility};
}

const ServiceContract = (props) => {
    const { setActiveIndex } = props;
    const {isVisible, togglePasswordVisibility} = useTogglePasswordVisibiliy();
    const {openDialog, switchDialog} = useActionsModal();

    return (
        <div>

            <div className="card">
                <ScrollPanel style={{ marginLeft: '-18%', width: '140%', height: '300px' }} className="custombar1">
                    {/* <div style={{ padding: '1rem', lineHeight: '1.5' }}> */}
                    <div>
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through
                        Michael's life the nature of the family business becomes clear. The business of the family is just like the head
                        of the family, kind and benevolent to those who give respect, but given to ruthless violence whenever anything stands
                        against the good of the family.
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through Michael's
                        life the nature of the family business becomes clear. The business of the family is just like the head of the family, kind
                        and benevolent to those who give respect, but given to ruthless violence whenever anything stands against the good of the
                        family.
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through
                        Michael's life the nature of the family business becomes clear. The business of the family is just like the head
                        of the family, kind and benevolent to those who give respect, but given to ruthless violence whenever anything stands
                        against the good of the family.
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through Michael's
                        life the nature of the family business becomes clear. The business of the family is just like the head of the family, kind
                        and benevolent to those who give respect, but given to ruthless violence whenever anything stands against the good of the
                        family.
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through
                        Michael's life the nature of the family business becomes clear. The business of the family is just like the head
                        of the family, kind and benevolent to those who give respect, but given to ruthless violence whenever anything stands
                        against the good of the family.
                        The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughter's wedding. His beloved
                        son Michael has just come home from the war, but does not intend to become part of his father's business. Through Michael's
                        life the nature of the family business becomes clear. The business of the family is just like the head of the family, kind
                        and benevolent to those who give respect, but given to ruthless violence whenever anything stands against the good of the
                        family.
                    </div>
                </ScrollPanel>
            </div>

            <div className="p-pt-6">
                <div className="formInputs p-d-flex p-ai-center p-jc-center">
                    <span className="p-float-label p-mb-0">
                        <InputText type={isVisible ? "text" : "password"} id="kid-curp" className="p-inputtext-sm" />
                        <label htmlFor="curp">Contraseña*</label>
                    </span>
                    <i className={(isVisible ? "pi pi-eye-slash" : "pi pi-eye") + " password-toggle-icon"} onClick={togglePasswordVisibility}></i>
                </div>
                <div className="kids-form p-d-flex p-flex-column p-jc-center p-ai-center">
                    <Button className="p-mt-5" onClick={switchDialog}>Aceptar</Button>
                </div>
            </div>
            {/* <Button>Continuar</Button> */}

            <Modal header={"Aviso"} visible={openDialog} onHide={switchDialog} action={() => setActiveIndex(4)} canClose={false} actionBtn="Continuar">
                <p>¡Felicidades! Manuelito ya cuenta con Afore.</p>
            </Modal>
        </div>
    )
}

export default ServiceContract