import React from 'react';
import { InputText } from 'primereact/inputtext';

const ComplementaryData = (props) => {

    return (
        <div>
            <div className="formInputs p-d-flex p-ai-center ">
                <span className="p-float-label p-mb-0">
                    <InputText id="kid-curp" className="p-inputtext-sm" />
                    <label htmlFor="curp">Parentesco*</label>
                </span>
            </div>
            <div className="formInputs p-d-flex p-ai-center p-mt-3">
                <span className="p-float-label p-mb-0">
                    <InputText id="kid-curp" className="p-inputtext-sm" />
                    <label htmlFor="curp">Asignar un alias*</label>
                </span>
            </div>
            <div className="kids-obligatorio">
                <label htmlFor="obligatorio">*Obligatorio</label>
            </div>
        </div>
    )
}

export default ComplementaryData;
