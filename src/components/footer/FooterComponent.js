import React, { Component } from 'react'
import './footer.scss';
import consar from './../../assets/img/consar.svg'
import sartel from './../../assets/img/sartel1.svg'
import sarteleua from './../../assets/img/consareua1.svg'
import secure from './../../assets/img/ssl1.svg'

import face from './../../assets/img/face.svg'
import twiter from './../../assets/img/twit.svg'
import insta from './../../assets/img/insta.svg'
import youtube from './../../assets/img/yout.svg'

const FooterComponent = (props) => {
    return(
        <>
            <div className="footer-container">
                <div className="p-grid">
                    <div className="p-col-12 p-md-4 footer-side-left">
                        <div className="p-grid footer-left">
                            <div className="p-col-12">
                                <span className="title-item-services ">Nuestros Servicios</span>
                            </div>
                            <div className="p-col-6 item-services">
                                Avisos COVID 19
                            </div>
                            <div className="p-col-6 item-services">
                                Mi Perfil
                            </div>
                            <div className="p-col-6 item-services">
                                Responsabilidad Social
                            </div>
                            <div className="p-col-6 item-services">
                                Calculadora
                            </div>
                            <div className="p-col-6 item-services">
                                Glosario
                            </div>
                            <div className="p-col-5 item-services">
                                Regístrate en una Afore
                            </div>
                            <div className="p-col-6 item-services">
                                Mapa de Sitio
                            </div>
                            <div className="p-col-6 item-services">
                                Contáctanos
                            </div>
                            <div className="p-col-6 item-services">
                                Términos y condiciones
                            </div>
                            <div className="p-col-6 item-services">
                                Aviso de Privacidad
                            </div>
                        </div>
                    </div>
                    <div className="p-col-12 p-md-6 footer-side-center">
                        <div className="p-grid footer-left">
                            <div className="p-col-12 ">
                                <span className="title-item-services  consar-fuente">
                                En CONSAR estamos para ayudarte, si tienes dudas o aclaraciones contáctanos a través de los siguientes medios:</span>
                            </div>
                            <div className="p-col-12 p-md-4">
                                <img height="60px" src={consar}/>
                                    </div>
                            <div className="p-col-6 p-md-4">
                                <img height="60px" src={sartel}/>
                                    </div>
                            <div className="p-col-6 p-md-4">
                                <img height="60px" src={sarteleua}/>

                                    </div>
                            <div className="p-col-12 consar-fuente2 ">
                                Los datos personales que se recaban de usted en este portal, serán utilizados para que pueda hacer uso de los servicios del Sistema de Ahorro para el Retiro que se encuentran disponibles en el portal. Si tiene alguna duda  técnica puede consultarla en la siguiente dirección: atencionpublico@e-sar.com.mx <br/>
                                Copyright 2015 e-SAR - Todos los derechos reservados
                            </div>
                        </div>
                    </div>
                    <div className="p-col-12 p-md-2 footer-side-right">
                        <div className="p-grid footer-left">
                            <div className="p-col-12">
                                <span className="title-item-services  redes-fuente">Nuestras redes</span>
                            </div>
                            <div className="p-col-3 secure-img3 ">
                                <img height="20px" src={face}/>
                            </div>
                            <div className="p-col-3 secure-img3 ">
                                <img height="20px" src={twiter}/>
                            </div>
                            <div className="p-col-3 secure-img3 ">
                                <img height="20px" src={insta}/>
                            </div>
                            <div className="p-col-3  secure-img3">
                                <img height="20px" src={youtube}/>
                            </div>
                            <div className="p-col-12 secure-img">
                                <img height="50px" src={secure}/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default FooterComponent
