import React from 'react';
import { Button } from './../misc/button/Button';
import { InputText } from 'primereact/inputtext';
// Media and styles
import './Login.scss';

const SecurityCode = (props) => {
    return (
        <div className="form p-d-flex p-flex-column p-jc-center p-ai-center">
            <div className="formHeader p-mb-6">
                <h1 className="title p-mb-3">Generación de contraseña</h1>
                <span className="info">Lorem ipsm dolor sit amet, consectetuer adipiscing elit, sed diam nonum-my nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</span>
            </div>

            <div className="formInputs securityCode p-d-flex p-flex-column p-jc-center p-ai-center p-mt-6 p-mb-6">
                <span className="title p-mb-4">Ingresa tu código de seguridad</span>
                <div className="securityCodeInputs p-d-flex p-mb-3">
                    <InputText keyfilter="int" className="p-mr-3" placeholder="*" maxlength="1" />
                    <InputText keyfilter="int" className="p-mr-3" placeholder="*" maxlength="1"/>
                    <InputText keyfilter="int" className="p-mr-3" placeholder="*" maxlength="1"/>
                    <InputText keyfilter="int" className="" placeholder="*" maxlength="1"/>
                </div>
                <a href="" className="a-secCode">Reenviar código de seguridad</a>
            </div>
            <Button onClick={props.nextStep}>Continuar</Button>
        </div>
    )
}
export { SecurityCode }