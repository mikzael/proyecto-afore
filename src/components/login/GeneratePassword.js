import React, { useState } from 'react';
import { Button } from '../misc/button/Button';
import { Password } from 'primereact/password';
import { Modal } from '../misc/modal/Modal';
// Media and styles
import './Login.scss';

const GeneratePassword = (props) => {
    const [password, setPassword] = useState(null);
    const [confirmPassword, setConfirmPassword] = useState(null);
    const [openDialogCompleted, setOpenDialogCompleted] = useState(false);

    // Dialog path completed
    const handleOpenDialogCompleted = () => {
        setOpenDialogCompleted(true)
    }

    const handleCloseDialogCompleted = () => {
        setOpenDialogCompleted(false)
    }

    return (
        <div className="form p-d-flex p-flex-column p-jc-center p-ai-center">
            <div className="formHeader p-mb-6">
                <h1 className="title p-mb-2">Generación de contraseña</h1>
                <span className="info">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam</span>
            </div>

            <div className="formInputs p-d-flex p-flex-column p-ai-center p-mb-6">
                <span className="p-float-label p-mb-5">
                    <Password id="password" value={password} onChange={(e) => setPassword(e.target.value)} toggleMask feedback={false}/>
                    <label htmlFor="password">Contraseña*</label>
                </span>
                <span className="p-float-label p-mb-6">
                    <Password id="confirmPassword" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} toggleMask feedback={false} />
                    <label htmlFor="confirmPassword">Confirma tu Contraseña*</label>
                </span>
                <div id="GP-feedback" className="paswordStrength p-mb-5 p-d-flex p-ai-center">
                    <div className="p-password-meter">
                        <div className="p-password-strength strong" style={{width: "33.33%"}}></div>
                    </div>
                    <i className="pi pi-lock p-ml-2"></i>
                </div>
                <div className="advices">
                    <div className="advice p-mb-5">
                        <div className="p-mb-2">La contraseña debe tener:</div>
                        <div className="info p-d-flex p-jc-center">
                            <span>De 8 a 12 caracteres</span>
                            <span>Alfanumérica</span>
                        </div>
                    </div>
                    <div className="advice">
                        <div className="p-mb-2">La contraseña no debe tener:</div>
                        <div className="info p-d-flex">
                            <span>Únicamente un caracter (aaaaa11111)</span>
                            <span>Caracteres consecutivos (12345678)</span>
                        </div>
                    </div>
                </div>
            </div>
            <Button onClick={handleOpenDialogCompleted}>Continuar</Button>

            <Modal action={handleCloseDialogCompleted} header={"¡Felicidades!"} visible={openDialogCompleted}  canClose={false} actionBtn="Aceptar">
                <p>Tu usuario se ha creado exitosamente, a continuación recibirás un correo de bienvenida.</p>
            </Modal>
        </div>
    )
}
export { GeneratePassword }