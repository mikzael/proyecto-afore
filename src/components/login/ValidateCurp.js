import React, { useState } from 'react';
import { Button } from '../misc/button/Button';
import { Avatar } from 'primereact/avatar';
import { InputText } from 'primereact/inputtext';
import { Modal } from '../misc/modal/Modal';
import { ModalAccept } from '../misc/modal/ModalAccept';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch,
} from "react-router-dom";
// Media and styles
import './Login.scss';

const ValidateCurp = (props) => {
    let { path, url } = useRouteMatch();

    const [openDialogCurp, setOpenDialogCurp] = useState(false);
    const [openModalPrivacy, setOpenModalPrivacy] = useState(false);

    // Dialog CURP not registered
    const handleOpenDialogCurp = () => {
        setOpenDialogCurp(true)
    }

    const handleCloseDialogCurp = () => {
        setOpenDialogCurp(false)
    }
    // Accept privacy policy modal
    const handleOpenModalPrivacy = () => {
        handleCloseDialogCurp();
        setOpenModalPrivacy(true)
    }

    const handleCloseModalPrivacy = () => {
        setOpenModalPrivacy(false)
    }


    return (
        <div className="form p-d-flex p-flex-column p-jc-center p-ai-center">
            <div className="formHeader p-mb-5">
                <h1 className="title p-mb-1">Valida tu CURP</h1>
            </div>

            <div className="formInputs p-d-flex p-ai-center p-jc-center p-mb-6">
                <Avatar image="" className="p-mr-2" size="large" shape="circle" />
                <span className="p-float-label p-mb-0">
                    <InputText id="curp" />
                    <label htmlFor="curp">CURP</label>
                </span>
            </div>
            <Button onClick={handleOpenDialogCurp}>
                Validar CURP
            </Button>

            <Modal header={"Aviso"} visible={openDialogCurp} onHide={handleCloseDialogCurp} action={handleOpenModalPrivacy} canClose={true} actionBtn="Regístrate" cancelBtn="Cancelar">
                <p>La CURP que ingresaste no está registrada. Iniciaremos tu proceso de registro.</p>
            </Modal>

            <ModalAccept action={props.nextStep} header={"Aviso de privacidad Afore Web"} visible={openModalPrivacy} onHide={handleCloseModalPrivacy} acceptBtn="He leído y acepto el Aviso de Privacidad.">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blan-dit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
            </ModalAccept>
        </div>
    )
}
export { ValidateCurp }