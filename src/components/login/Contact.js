import React, { useState } from 'react';
import { Button } from '../misc/button/Button';
import { InputText } from 'primereact/inputtext';
import { InputMask } from 'primereact/inputmask';
import { AutoComplete } from 'primereact/autocomplete';
import { Modal } from '../misc/modal/Modal';
import {
    useRouteMatch,
} from "react-router-dom";
// Media and styles
import './Login.scss';

const Contact = (props) => {
    let { path, url } = useRouteMatch();
    
    const [openDialogSMS, setOpenDialogSMS] = useState(false);
    
    // Dialog when SMS was sended
    const handleOpenDialogSMS = () => {
        setOpenDialogSMS(true)
    }

    const handleCloseDialogSMS = () => {
        setOpenDialogSMS(false)
    }

    return (
        <div className="form p-d-flex p-flex-column p-jc-center p-ai-center">
            <div className="formHeader p-mb-5">
                <h1 className="title p-mb-1">Datos de Contacto</h1>
                <span className="subtitle">Ingresa tus datos</span>
            </div>

            <div className="formInputs p-d-flex p-flex-column p-ai-center p-mb-6">
                <span className="p-float-label  p-mb-5">
                    <InputText id="mail" />
                    <label htmlFor="mail">Correo*</label>
                </span>
                <span className="p-float-label  p-mb-5">
                    <InputText id="confirmMail" />
                    <label htmlFor="confirmMail">Confirma tu Correo*</label>
                </span>

                <span className="p-float-label  p-mb-5">
                    <AutoComplete id="country" field="name" />
                    <label htmlFor="country">País*</label>
                </span>

                <span className="p-float-label  p-mb-5">
                    <InputMask id="phone" mask="(999) 999-9999"></InputMask>
                    <label htmlFor="phone">Teléfono*</label>
                </span>
                <span className="instructions">*Obligatorio</span>
            </div>
            <Button onClick={handleOpenDialogSMS}>Continuar</Button>

            <Modal action={props.nextStep} header={"Aviso"} visible={openDialogSMS} onHide={handleCloseDialogSMS} canClose={false} actionBtn="Aceptar">
                <p>Hemos enviado un Código de Seguridadvía SMS al número que ingresaste.</p>
            </Modal>
        </div>
    )
}
export { Contact };