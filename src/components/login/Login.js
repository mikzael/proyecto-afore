import React, { useState } from 'react';
import { LateralBanner } from '../misc/lateralBanner/LateralBanner';
import { ValidateCurp } from './ValidateCurp';
import { Contact } from './Contact';
import { SecurityCode } from './SecurityCode';
import { GeneratePassword } from './GeneratePassword';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    useRouteMatch,
} from "react-router-dom";
// Helpers
import useDocumentTitle from '../../helpers/useDocumentTitle';

// Media and styles
import './Login.scss';

const Login = (props) => {
    useDocumentTitle("Inicio de Sesión");
    let { path, url } = useRouteMatch();

    const [step, setStep] = useState(1);

    const NextStep = (newRoute) => {
        setStep(step + 1);
        props.history.push(`${url}/${newRoute}`)
    }

    return (
        <section className="login">
            <div className="p-d-flex">
                <div className="loginL p-col-9">
                    <Switch>
                        <Route exact path={path}>
                            <ValidateCurp {...props} nextStep={() => NextStep('contacto')} step={step} />
                        </Route>
                        <Route path={`${path}/contacto`}>
                            <Contact {...props} nextStep={() => NextStep('codigo-seg')} step={step} />
                        </Route>
                        <Route path={`${path}/codigo-seg`}>
                            <SecurityCode {...props} nextStep={() => NextStep('contraseña')} />
                        </Route>
                        <Route path={`${path}/contraseña`}>
                            <GeneratePassword />
                        </Route>
                        <Route path={`${path}/*`}>
                            <Redirect to={path} />
                        </Route>
                    </Switch>
                </div>
                <div className="p-col-3">
                    <LateralBanner />
                </div>
            </div>

        </section>
    )
}
export default Login;