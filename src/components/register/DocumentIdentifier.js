import React, {  useRef, useState } from 'react';
// Media and styles
import './Register.scss';
import './Temp.scss';

import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { RadioButton } from 'primereact/radiobutton';
import { FileUpload } from 'primereact/fileupload';
import { ProgressBar } from 'primereact/progressbar';
import { Tooltip } from 'primereact/tooltip';
import { Tag } from 'primereact/tag';
import { Toast } from 'primereact/toast';
import ine from './../../assets/img/ine.svg'
import FileUploader from './../misc/inputs/FileUploader';
import { Modal } from '../misc/modal/Modal';
import { ModalAccept } from '../misc/modal/ModalAccept';
import {Checkbox} from 'primereact/checkbox';
import camara from './../../assets/img/camara.svg'
import { Dialog } from 'primereact/dialog';

const DocumentIdentifier = (props) => {
    const [documentSelected, setDocumentSelected] = useState(null);
    const [selectedFile, setSelectedFile] = useState(null);
    const [aceptarTerminos, setAceptarTerminos] = useState(false);
    const [openModalTerminos, setOpenModalTerminos] = useState(false); 
 
    // On file select (from the pop up)
    const onFileChange = event => {
        // Update the state
        const file = event.target.files[0];
        // User cancelled
        if (!file) {
            return
        }
        setSelectedFile(file);
    };

    // On file upload (click the upload button)
    const onFileUpload = () => {
        // Create an object of formData
        const formData = new FormData();

        // Update the formData object
        formData.append(
            "myFile",
            selectedFile,
            selectedFile.name
        );

        // Details of the uploaded file
        console.log(selectedFile);

        // Request made to the backend api
        // Send formData object
        //axios.post("api/uploadfile", formData);
    };
    const handleSubmission = () => {
        const formData = new FormData();

        formData.append('File', selectedFile);

        fetch(
            'https://freeimage.host/api/1/upload?key=<YOUR_API_KEY>',
            {
                method: 'POST',
                body: formData,
            }
        )
            .then((response) => response.json())
            .then((result) => {
                console.log('Success:', result);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    };



    return (<>
            <div className="p-grid documentos-container">
                <div className="p-col-8">
                    <div className="p-grid texto-centrado">
                        <div className="p-col-12 item-identificacion">
                            Documento de identificación
                        </div>
                        <div className="p-col-12 tipo-identificacion">
                            Porfavor selecciona el tipo de documento con que deseas identificarte.
                        </div>
                        <div className="p-col-12">
                            <div className="p-grid">
                                <div className="p-col-5"/>
                                <div className="p-col-4 texto-negrita">
                                    <div className="p-field-radiobutton">
                                        <RadioButton onChange={(e) => setDocumentSelected(e.value)} inputId="txtIne" name="document" value="INE / IFE"
                                                     checked={documentSelected === 'INE / IFE'}/>
                                        <label htmlFor="txtIne"> INE / IFE</label>
                                    </div>
                                </div>
                                <div className="p-col-3"/>

                                <div className="p-col-5"/>
                                <div className="p-col-4 texto-negrita">
                                    <div className="p-field-radiobutton">
                                        <RadioButton onChange={(e) => setDocumentSelected(e.value)} inputId="txtMatricula" name="document" value="Matrícula Consular"
                                                     checked={documentSelected === 'Matrícula Consular'}/>
                                        <label htmlFor="txtMatricula">Matrícula Consular</label>
                                    </div>
                                </div>
                                <div className="p-col-3"/>

                                <div className="p-col-5"/>
                                <div className="p-col-4 texto-negrita">
                                    <div className="p-field-radiobutton">
                                        <RadioButton onChange={(e) => setDocumentSelected(e.value)} inputId="txtPasaporte" name="document" value="Pasaporte"
                                                     checked={documentSelected === 'Pasaporte'}/>
                                        <label htmlFor="txtPasaporte">Pasaporte</label>
                                    </div>
                                </div>
                                <div className="p-col-3"/>

                                <div className="p-col-5"/>
                                <div className="p-col-4 texto-negrita">
                                    <div className="p-field-radiobutton">
                                        <RadioButton onChange={(e) => setDocumentSelected(e.value)} inputId="txtForma" name="document" value="Forma Migratoria"
                                                     checked={documentSelected === 'Forma Migratoria'}/>
                                        <label htmlFor="txtForma">Forma Migratoria</label>
                                    </div>
                                </div>
                                <div className="p-col-3"/>

                                <div className="p-col-12 tipo-identificacion">
                                Adjunta el documento elegido.
                                </div>

                                <div className="p-col-3 icon-bottom texto-derecha centrado">
                                    <img height="60px" src={ine}/>
                                </div>
                                <div className="p-col-6 centrado">
                                    <div className="p-fluid p-formgrid p-grid texto-derecha ">
                                        <div className="p-field p-col">
                                            <InputText/>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-col-3 centrado">
                                    <FileUploader classComponent="p-button-secondary" handleFile={onFileChange} label="Buscar" />
                                </div>

                                <div className="p-col-12 texto-centrado icon-bottom">
                                    <Button label="Continuar" className="p-button-secondary"/>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-col-4"/>
            </div>

            <div className="p-grid documentos-container">
                <div className="p-col-8">
                    <div className="p-grid texto-centrado">
                        <div className="p-col-12 item-identificacion">
                            Ahora tomaremos un video                           
                        </div>
                        <div className="p-col-12 texto-centrado">
                            <div className="p-col-12 icon-bottom texto-derecha centrado">
                                    <img height="190px" src={camara}/>
                            </div>
                        </div>
                        <div className="p-col-3"></div>
                        <div className="p-col-6 tipo-video texto-centrado">
                            Con la finalidad de proteger tu identidad, a continuación te pediremos que grabes un video,
                            en el que expreses que es tu voluntad registrarte a la administradora
                            <span className="texto-negrita"> (nombre administradora)  </span>
                        </div>
                        <div className="p-col-3"></div>

                        <div className="p-col-12 texto-centrado icon-bottom">
                            <Button onClick={()=>setOpenModalTerminos(true)} label="Continuar" className="p-button-secondary"/>
                        </div>
                    
                        <Dialog header={""} visible={openModalTerminos}>
                        <div className="p-grid documentos-container">
                        <div className="p-col-12 item-identificacion texto-centrado"> Carta de derechos </div>   
                        <div className="p-col-2 "> </div> 
                        <div className="p-col-8 centrado">
                        <div className="p-field-checkbox  tipo-video">
                        <Checkbox inputId="binary" checked={aceptarTerminos} onChange={e => setAceptarTerminos(e.checked)} />
                        <label htmlFor="binary">He leído y acepto la Carta de Derechos   </label>  
                        </div> 
                        </div>
                        <div className="p-col-2"></div>
                        </div>   
                        </Dialog>
                    </div>
                </div>
            </div>
        </>

)
}
export default DocumentIdentifier
