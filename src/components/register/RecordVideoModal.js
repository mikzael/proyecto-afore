import React, { useState, useEffect } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { SplitButton } from 'primereact/splitbutton';
// Media and styles
import './Temp.scss';
import Record from '../../assets/img/record.png'

const RecordVideoModal = (props) => {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const renderFooter = (action) => {
        return (
            <div className="p-d-flex  p-ai-center">
                <div className="permissionsButtons p-col-5 p-d-flex">
                    <SplitButton icon="pi pi-video" className="p-button-text p-button-sm"></SplitButton>
                    <SplitButton icon="pi pi-video" className="p-button-text p-button-sm"></SplitButton>
                </div>
                <div className="p-col-2 p-d-flex p-jc-center">
                    <div className="recordButton">
                        <img src={Record}></img>
                    </div>
                </div>
                <div className="p-col-5 actionButton p-d-flex p-jc-end">
                    <Button label="Grabar de nuevo" onClick={action} autoFocus />
                </div>
            </div>
        );
    }

    return (
        <Dialog className='dialogAfore recordVideoModal' header={props.header} visible={visible} footer={() => renderFooter(props.action)} onHide={() => setVisible(false)} draggable={false} resizable={false}>
            <div className="videoContainer"></div>
        </Dialog>
    )
}

export default RecordVideoModal;