import React from 'react';

// Media and styles
import './Register.scss';
import './Temp.scss';
// Helpers
import useDocumentTitle from '../../helpers/useDocumentTitle';
// Components
import ServiceContract from './ServiceContract';
import {LateralBanner} from '../misc/lateralBanner/LateralBanner';
import RecordVideoModal from './RecordVideoModal';

const Register = (props) => {
    useDocumentTitle("Registro");

    return (
        <section className="register">
            <div className="p-d-flex">
                <div className="p-col-9">
                    <ServiceContract />
                </div>

                <div className="p-col-3">
                    <LateralBanner />
                </div>
            </div>
        </section>
    )
}
export default Register;