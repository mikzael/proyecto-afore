import React from "react";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { InputMask } from "primereact/inputmask";
import { AutoComplete } from 'primereact/autocomplete';
import { CascadeSelect } from 'primereact/cascadeselect';
import { Dropdown } from 'primereact/dropdown';

// Media and styles
import "./Register.scss";
import '../login/Login.scss';

const AdditionalData = (props) => {
  return (
    <div>
      <div className="form p-d-flex p-col-8 p-flex-column p-jc-center p-ai-center">
        <div className="header">
        <h1 className="title p-mb-1">
          Por último proporciona ésta información.
        </h1>
        </div>
        <div className="formInputs p-d-flex p-flex-column p-mb-6">
          <span className="p-float-label  p-mb-4">
            <InputText id="country" disabled placeholder="México" style={{background: '#e3e3e3', borderRadius: '10px'}} />
          </span>
          <span className="p-float-label  p-mb-4">
            <InputText id="zipcode" />
            <label htmlFor="zipcode">Código Postal*</label>
          </span>
          <span className="p-float-label  p-mb-4">
            <InputText id="mail" />
            <label htmlFor="mail">Entidad Federativa*</label>
          </span>
          <span className="p-float-label  p-mb-4">
            <InputText id="mail" />
            <label htmlFor="mail">Municipio / Delegación*</label>
          </span>
          <span className="p-float-label  p-mb-4">
          <Dropdown id="colonia" style={{background: '#e3e3e3', borderRadius: '10px'}} />
            <label htmlFor="colonia">Colonia*</label>
          </span>
          <span className="p-float-label  p-mb-4">
            <AutoComplete id="street" field="name" />
            <label htmlFor="street">Calle*</label>
          </span>
          <span className="p-float-label  p-mb-4">
            <InputMask id="phone" mask="(999) 999-9999"></InputMask>
            <label htmlFor="phone">Teléfono particular*</label>
          </span>
          <span className="p-float-label  p-mb-4">
            <Dropdown id="actividad" style={{background: '#e3e3e3', borderRadius: '10px'}} />
            <label htmlFor="actividad">Actividad Económica*</label>
          </span>
          <span className="instructions">*Obligatorio</span>
        </div>
        <Button onClick={props.action}>Continuar</Button>
      </div>
    </div>
  );
};

export default AdditionalData;
