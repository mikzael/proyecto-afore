import React, {  useRef, useState } from 'react';
// Media and styles
import './Register.scss';
import './Temp.scss';
import {Checkbox} from 'primereact/checkbox';
import camara from './../../assets/img/camara.svg'
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Modal } from '../misc/modal/Modal';
const RecordVideoInstructions = (props) => {

    const [aceptarTerminos, setAceptarTerminos] = useState(false);
    const [openModalTerminos, setOpenModalTerminos] = useState(false); 

    const acceptTerms =()=>{


    }

    const handleClose = () =>{


    }

    return (<>
            <div className="p-grid documentos-container">
                <div className="p-col-8">
                    <div className="p-grid texto-centrado">
                        <div className="p-col-12 item-identificacion">
                            Ahora tomaremos un video                           
                        </div>
                        <div className="p-col-12 texto-centrado">
                            <div className="p-col-12 icon-bottom texto-derecha centrado">
                                    <img height="190px" src={camara}/>
                            </div>
                        </div>
                    
                        <div className="p-col-12 tipo-video texto-centrado margin_texto">
                            Con la finalidad de proteger tu identidad, a continuación te pediremos que grabes un video,
                            en el que expreses que es tu voluntad registrarte a la administradora <br /> 
                            <span className="texto-negrita"> (nombre administradora)  </span>
                        </div>
                   

                        <div className="p-col-12 texto-centrado icon-bottom">
                            <Button onClick={()=>setOpenModalTerminos(true)} label="Continuar" className="p-button-secondary"/>
                        </div>
                        <Modal header={"Carta de derechos"} visible={openModalTerminos} onHide={handleClose} action={acceptTerms} canClose={true} actionBtn="Aceptar">
                        <div className="p-grid documentos-container">
                            <div className="p-col-12 item-identificacion texto-centrado"> Carta de derechos </div>   
                          
                            <div className="p-col-12 centrado">
                                <div className="p-field-checkbox  tipo-video">
                                <Checkbox inputId="binary" checked={aceptarTerminos} onChange={e => setAceptarTerminos(e.checked)} />
                                <label htmlFor="binary">He leído y acepto la Carta de Derechos   </label>  
                                </div> 
                            </div>
                          
                            </div>   
                        </Modal>
                        {/* <Dialog header={""} visible={openModalTerminos}>
                            <div className="p-grid documentos-container">
                            <div className="p-col-12 item-identificacion texto-centrado"> Carta de derechos </div>   
                            <div className="p-col-2 "> </div> 
                            <div className="p-col-8 centrado">
                            <div className="p-field-checkbox  tipo-video">
                            <Checkbox inputId="binary" checked={aceptarTerminos} onChange={e => setAceptarTerminos(e.checked)} />
                            <label htmlFor="binary">He leído y acepto la Carta de Derechos   </label>  
                            </div> 
                            </div>
                            <div className="p-col-2"></div>
                            </div>   
                        </Dialog> */}





                    </div>
                </div>
                <div className="p-col-4"></div>
            </div>
        </>
    )
}


export default RecordVideoInstructions
