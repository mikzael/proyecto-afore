import React, { useState } from 'react';
// Media and styles
import './Register.scss';
import './Temp.scss';
import { Button } from 'primereact/button';
import { Rating } from 'primereact/rating';
// Afore images
import AforeCoppel from '../../assets/img/afores/afore-coppel.png'

const getAforeImg = (afore) => {
    return AforeCoppel;
}

const AforeInfo = (props) => {
    return (
        <div className="aforeInfo">
            <div className="header-register p-mb-5 p-text-center">
                <h1 className="title p-mt-0 p-mb-5">Rendimientos, comisiones y servicios de la Afore</h1>
                <div className="aforeImg p-mb-4">
                    <img src={getAforeImg(props.afore)} />
                </div>
                <span className="subtitle">Te estas registrando en {props.afore ? props.afore : "Afore"} que te ofrece: </span>
            </div>

            <div className="p-d-flex p-flex-column">
                <div className="p-d-flex p-mb-3">
                    <div className="infoSquare p-col p-mr-4">
                        <div className="head">
                            <h3>Comisión</h3>
                        </div>
                        <div className="content p-d-flex p-flex-column p-ai-center p-jc-center">0.98%</div>
                    </div>
                    <div className="infoSquare p-col p-mr-4">
                        <div className="head">
                            <h3>Rendimiento Neto*</h3>
                        </div>
                        <div className="content p-d-flex p-flex-column p-ai-center p-jc-center">5.22%</div>
                    </div>
                    <div className="infoSquare p-col">
                        <div className="head">
                            <h3>Servicios**</h3>
                        </div>
                        <div className="content p-d-flex p-flex-column p-ai-center p-jc-center">
                            <Rating value={2} readOnly stars={5} cancel={false}/>
                        </div>
                    </div>
                </div>
                <div className="conditions p-mb-6">
                    <p className="p-mt-0 p-mb-2">*Rendimientos netos vigentes al día del mes. Rendimeintos pasados no garantizan rendimientos futuros.</p>
                    <p className="p-mt-0">**Indicador + Afore al cierre de mes de año.</p>
                </div>
                <Button className="p-as-center">Continuar</Button>
            </div>
        </div>
    )
}
export default AforeInfo