import React from "react";
// Media and styles
import "./Register.scss";

import banorteIcon from "../../assets/img/afores/banorte.svg";
import profuturoIcon from "../../assets/img/afores/profuturo.svg";
import suraIcon from "../../assets/img/afores/sura.svg";
import inbursaIcon from "../../assets/img/afores/inbursa.svg";
import invercapIcon from "../../assets/img/afores/invercap.svg";
import pensionisssteIcon from "../../assets/img/afores/pensionissste.svg";
import principalIcon from "../../assets/img/afores/principal.svg";
import citibanamexIcon from "../../assets/img/afores/citibanamex.svg";
import aztecaIcon from "../../assets/img/afores/azteca.svg";
import coppelIcon from "../../assets/img/afores/coppel.svg";

const SelectAfore = () => {
  return (
    <>
    <div className="p-d-inline-block p-grid p-col-8 p-ml-6 p-justify-center">
      <h1 className="title p-mb-1">Selecciona la AFORE a la que deseas registrarte:</h1>
        <p className="subtitle">Instrucciones pendientes.</p>
      </div>
      <div className="p-grid p-col-8 p-ml-6 p-justify-center">
        <div className="p-col-4 afores">
          <img
          src={ banorteIcon }
          alt=""
          height="90"
          className=""
          />
        </div>
        <div className="p-col-4 afores">
          <img
          src={ profuturoIcon }
          alt=""
          height="90"
          className=""
          />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ suraIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ inbursaIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ invercapIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ pensionisssteIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ principalIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ citibanamexIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ aztecaIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
        <div className="p-col-4 afores">
        <img
          src={ coppelIcon }
          alt=""
          height="90"
          className=""
        />
        </div>
      </div>
    </>
  );
};

export default SelectAfore;
