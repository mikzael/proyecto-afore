import React, { useState } from 'react';
// Media and styles
import './Register.scss';
import './Temp.scss';

import { InputNumber } from 'primereact/inputnumber';
import IMSS from '../../assets/img/IMSS.png'

const NSS = (props) => {
    const [nss, setNss] = useState(null);

    return (
        <div>
            <div className="header-register p-mb-6">
                <h1 className="title p-mt-0 p-mb-2">Ingresa tus Datos</h1>
                <span className="subtitle">Proporciona tu numero de seguridad social</span>
            </div>

            <div className="p-d-flex p-flex-column">
                <div className="nssCard p-shadow-5 p-mb-5 p-d-flex p-ai-center">
                    <div className="nss-image p-mr-6"><img src={IMSS} /></div>
                    <div className="p-d-flex p-flex-column nss">
                        <span className="p-float-label p-input-filled nss">
                            <InputNumber id="nss" className="" aria-describedby="nss-help" value={nss} onValueChange={(e) => setNss(e.value)} mode="decimal" useGrouping={false} />
                            <label htmlFor="nss">Número de Seguridad Social</label>
                        </span>
                        <div className="p-d-flex">
                            {/*<small id="nss-help" className="p-error">Lorem ipsum message</small>*/}
                            <small className="info">Opcional</small>
                        </div>
                    </div>
                </div>
                <Button className="p-as-end">Continuar</Button>
            </div>
        </div>
    )
}
export default NSS