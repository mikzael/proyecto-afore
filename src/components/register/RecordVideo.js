import React from 'react';
// Media and styles
import "./Register.scss";

const RecordVideo = () => {
    return (
        <div className="p-d-inline-block p-grid p-col-8 p-ml-6 p-justify-center">
            <h1 className="title p-mb-1">Video Voluntad</h1>
            <div className="p-grid p-jc-center p-ai-center p-mx-auto rec">REC</div>
            <h4 className="p-grid p-justify-center">Pulsa para grabar</h4>
            <p className="subtitle">En este proceso te guiaremos para completar fácilmente tu
            autenticación. <br /> Por favor lee el siguiente texto mientras grabas tu video.</p>
            <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed <br/>
            diam nonummy nibh euismod tincidunt ut laoreet dolore <br/> magna aliquam erat volutpat.
            Ut wisi enim ad minim veniam, <br/> quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut.</h2>
        </div>
    );
};

export default RecordVideo;
