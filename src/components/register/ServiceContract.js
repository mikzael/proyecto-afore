import React, { useState } from 'react';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import { Checkbox } from 'primereact/checkbox';
// Media and styles
import './Register.scss';
import './Temp.scss';

const ServiceContract = (props) => {
    const [password, setPassword] = useState(null);
    const [checked, setChecked] = useState(false);

    return (
        <div className="serviceContract">
            <div className="header-register p-mb-4 p-text-center">
                <h1 className="title p-mt-0 p-mb-2">Contrato de servicio</h1>
            </div>

            <div className="contratCard p-shadow-4 p-mb-3">
                <div className="overflow">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nisi erat, aliquet eu lacus ac, vulputate ornare lacus. Integer rutrum est id nulla malesuada, ac vehicula augue venenatis. Nullam malesuada tellus in malesuada sagittis. Sed luctus placerat odio a fermentum. In et fermentum leo. Phasellus in convallis mauris, eget dignissim odio. Aliquam cursus efficitur congue. Duis mauris tortor, mattis id molestie in, congue in erat. Mauris nec commodo est. Suspendisse eget nibh consectetur, luctus quam eget, hendrerit tortor. In ligula magna, sagittis ut sollicitudin eget, tempus eu dolor. Praesent imperdiet sodales nisl non egestas. Quisque odio nunc, blandit ac lorem vitae, consequat aliquet odio. Nulla facilisi. Aenean eget pharetra nunc, nec blandit lorem. Praesent varius lectus sed aliquam gravida.
                        Aliquam semper blandit aliquet. Fusce molestie, augue at tempor convallis, tellus nisi varius arcu, ut lobortis orci nibh sed elit. Nullam scelerisque pulvinar suscipit. Nunc dictum quis leo id lacinia. Vestibulum ullamcorper arcu a porttitor efficitur. Fusce eu venenatis purus, nec luctus quam. Donec est neque, vestibulum sed aliquet quis, luctus et est. Aliquam sed eros in dolor pretium condimentum. Sed ut erat ut eros pulvinar volutpat.
                        Maecenas cursus eros nec lorem placerat Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nisi erat, aliquet eu lacus ac, vulputate ornare lacus. Integer rutrum est id nulla malesuada, ac vehicula augue venenatis. Nullam malesuada tellus in malesuada sagittis. Sed luctus placerat odio a fermentum. In et fermentum leo. Phasellus in convallis mauris, eget dignissim odio. Aliquam cursus efficitur congue. Duis mauris tortor, mattis id molestie in, congue in erat. Mauris nec commodo est. Suspendisse eget nibh consectetur, luctus quam eget, hendrerit tortor. In ligula magna, sagittis ut sollicitudin eget, tempus eu dolor. Praesent imperdiet sodales nisl non egestas. Quisque odio nunc, blandit ac lorem vitae, consequat aliquet odio. Nulla facilisi. Aenean eget pharetra nunc, nec blandit lorem. Praesent varius lectus sed aliquam gravida.
                        Aliquam semper blandit aliquet. Fusce molestie, augue at tempor convallis, tellus nisi varius arcu, ut lobortis orci nibh sed elit. Nullam scelerisque pulvinar suscipit. Nunc dictum quis leo id lacinia. Vestibulum ullamcorper arcu a porttitor efficitur. Fusce eu venenatis purus, nec luctus quam. Donec est neque, vestibulum sed aliquet quis, luctus et est. Aliquam sed eros in dolor pretium condimentum. Sed ut erat ut eros pulvinar volutpat.
                        Maecenas cursus eros nec lorem placerat pharetra. Maecenas porta iaculis vestibulum. Donec sollicitudin porta enim, nec pellentesque eros tristique non. Sed molestie felis tempor arcu ultrices porttitor. Phasellus nisl ex, tempor quis ultrices vel, dictum ut arcu. Donec ut lacus urna. Vivamus tincidunt tortor sit amet dui tincidunt ultricies. Suspendisse potenti. Sed pellentesque mollis sem sed aliquam. Ut in vestibulum tellus. Suspendisse ornare lorem eu efficitur ultrices.
                    Sed sed commodo purus, vel porta augue. Phasellus enim sapien, suscipit ut facilisis eget, accumsan at odio. Duis feugiat, augue ac malesuada pulvinar, nunc odio varius ex, in scelerisque urna magna in nulla. Fusce in augue malesuada, mollis neque eget, tincidunt orci. Suspendisse maximus quis risus a fermentum. Aliquam erat volutpat. Aliquam eget sem sed nisi dapibus porta. Etiam lectus odio, aliquet sed consequat ut, bibendum et nisi. Cras posuere, ante in rutrum blandit, enim mi tempus augue, sit amet dictum turpis neque a dui. Vestibulum bibendum erat vel pretium mattis. Nulla rutrum id risus in sodales. Duis porttitor dapibus massa eu vestibulum. Integer sit amet dolor vel lacus malesuada dapibus. Curabitur sollicitudin placerat ultricies. Praesent enim sem, condimentum quis ultricies quis, fringilla lacinia eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas convallis nulla et massa rhoncus fermentum. Proin lobortis eget nunc quis elementum. Suspendisse mattis egestas lobortis. </p>
                </div>
            </div>
            <div className="p-d-flex p-flex-column p-ai-center">
                <Button label="Acepta aquí la carta de derechos*" className="p-button-link p-mb-3" />
                <div className="p-field-checkbox p-mb-5">
                    <Checkbox inputId="aceptar" checked={checked} onChange={e => setChecked(e.checked)}/>
                    <label htmlFor="aceptar" className="p-checkbox-label">He leído y acepto y acepto en contrato de servicio de (Afore)</label>
                </div>

                <span className="p-float-label p-mb-4">
                    <Password id="password" value={password} onChange={(e) => setPassword(e.target.value)} toggleMask feedback={false} />
                    <label htmlFor="password">Contraseña*</label>
                </span>
                <Button disabled={!checked && password !== null}>Continuar</Button>
            </div>
        </div>
    )
}
export default ServiceContract