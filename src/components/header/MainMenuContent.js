import React, { useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useHistory } from 'react-router-dom';
import { OverlayPanel } from 'primereact/overlaypanel';
import { colum1, colum2, colum3, colum4, colum5, columHelp1 } from './Constants';
// Styles
import './Header.scss';


function useRedirectTo(){
    const history = useHistory();
    const setRedirectTo = url => {
        if (url)
            history.push(url)
    }
    return { setRedirectTo }        
}

function ColumnRender({column}){
    let render = [];
    const {setRedirectTo} = useRedirectTo();

    column.forEach((item, i) => {
        if (item.type === 't'){
            render.push(
                <td key={i} className='main-menu-opt column-title'>
                        <div className="text-column-title">
                                    {item.content}
                        </div>
                </td>
            )
        }

        if(item.type === 'a'){
            render.push(
                <td key={i} className='main-menu-opt column-subtitle'>
                    <button className="text-column-subtitle" onClick={() => setRedirectTo(item.href)}>
                        {item.content}
                    </button>
                </td>
            )
        }

        if(item.type === 's'){
            render.push(
                <td key={i} className='main-menu-opt column-option'>
                    <button className="text-column-option" onClick={() => setRedirectTo(item.href)}>
                        {item.content}
                    </button>
                </td>
            )
        }
    })

    return render;
}

function ContentTable({columns}){
    let columsToRender = [];
    columns.forEach((column, i) => {
        if(i>0){
            columsToRender.push(
                <tr key={i} className="td-aling td-bar"><ColumnRender column={column}/></tr>
            )
        } else {
            columsToRender.push(
                <tr key={i} className="td-aling"><ColumnRender column={column}/></tr>
            )
        }
    })
    return columsToRender;

}


const MainMenuContent = (props) => {
    const op = useRef();
    const divChild = useRef();

    const {isOpen, eChild, clear, typeMenu} = props;

    useEffect(() => {
        const handleClickOutside = e => {
            const domNode = ReactDOM.findDOMNode(divChild.current);
            if (domNode !== null){
                if (!domNode.contains(e.target)){
                    clear();
                }
            }
        }

        document.addEventListener('mousedown', handleClickOutside, true);

        // if(isOpen === false){
        //     op.current.hide();
        // } else {
        //     if(eChild !== null)
        //         op.current.toggle(eChild)
        // }

        if(isOpen){
            if(eChild !== null){
                op.current.toggle(eChild)
            }
        } else {
            op.current.hide();
        }


        return () => {
            document.removeEventListener('mousedown', handleClickOutside, true)
        }
    }, [isOpen, eChild, clear]);

    return (
        <OverlayPanel ref={op}
            className={typeMenu !== null ?
                typeMenu === "m" ? 
                    "main-menu menu" : 
                    "main-menu space-help-menu" :
                "no-main-menu" }>
            <div className="columns-container" ref={divChild}>
                
                <table className='table-container'>
                    <tbody className='table-container'>
                        {typeMenu === 'm' ?
                            <ContentTable columns={[
                            colum1,
                            colum2,
                            colum3,
                            colum4,
                            colum5
                            ]}
                        />
                        :
                        <ContentTable columns={[
                            columHelp1,
                            ]}
                        />
                        }
                    </tbody>
                </table>
            </div>
        </OverlayPanel>
    )
}


export default MainMenuContent