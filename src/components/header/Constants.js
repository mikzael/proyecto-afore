export const colum1 = [
    {type: 't', content: 'Mi Perfil', href:''},
    {type: 'a', content: 'Actualización de datos', href:''}
]

export const colum2 = [
    {type: 't', content: 'Mi cuenta', href:''},
    {type: 'a', content: 'Consuta de saldos', href:'https://www.google.com.mx'},
    {type: 'a', content: 'Solicitud de documentos', href:''},
    {type: 'a', content: 'Consulta de semanas cotizadas', href:''},
    {type: 'a', content: 'Código de barras CURP', href:''},
    {type: 'a', content: 'Mis metas', href:''},
    /*{type: 'a', content: 'Emisión de carta de derechos', href:''},
    {type: 's', content: 'Consulta de semanas cotizadas', href:''},
    {type: 's', content: 'Código de barras CURP', href:''},
    {type: 's', content: 'Mis metas', href:''},*/
]

export const colum3 = [
    {type: 't', content: 'Mi ahorro', href:''},
    {type: 'a', content: 'Ahorro en línea', href:''},
    {type: 'a', content: 'Ahorro con beneficios', href:''},
    {type: 'a', content: 'Ahorro solidario', href:''},
    {type: 'a', content: 'Códigos de referencia', href:''},
    {type: 'a', content: 'Ahorro en efectivo', href:''},
]

export const colum4 = [
    {type: 't', content: 'Mis trámites', href:''},
    {type: 'a', content: 'Registro y consulta de menores', href:''},
    {type: 'a', content: 'Selección de SIEFORE', href:''},
    {type: 'a', content: 'Citas (CUS)', href:''},
    {type: 'a', content: 'Unificación SAR 92', href:''},
    {type: 'a', content: 'Unificación de recursos IMSS - ISSSTE', href:''},
    {type: 'a', content: 'Folio de registro o Traspaso', href:''},
    //{type: 'a', content: 'Identificación para tu traspaso', href:''},
    //{type: 'a', content: 'Folio de conocimiento del traspaso', href:''},
    {type: 'a', content: 'Cambio de Afore', href:''},
    {type: 'a', content: 'Atención y seguimiento de quejas', href:''},
    //{type: 's', content: 'Recertificación', href:''},
]

export const colum5 = [
    {type: 't', content: 'Mis retiros', href:''},
    {type: 'a', content: 'Retiro de aportaciones voluntarias', href:''},
    {type: 'a', content: 'Retiro total', href:''},
    {type: 't', content: 'Retiro programado', href:''},
    {type: 'a', content: 'Solicitud', href:''},
    {type: 'a', content: 'Eventos anuales de comprobación de supervivencia', href:''},
    {type: 't', content: 'Retiros parciales', href:''},
    {type: 'a', content: 'Retiro parcial por desempleo', href:''},
    //{type: 'a', content: 'Solicitud Pagos subsecuentes', href:''},
    //{type: 's', content: 'Retiro parcial por matrimonio', href:''},
    {type: 'a', content: 'Recuperación semanas de cotización', href:''},

]

export const columHelp1 = [
    {type: 't', content: 'Preguntas frecuentes', href:''},
    {type: 't', content: 'Indicadores de las Afores', href:''},
    {type: 'a', content: 'Índice de rendimiento neto', href:'https://me.com'},
    {type: 'a', content: 'Rendimiento a 12 meses', href:''},
    {type: 'a', content: 'Rendimiento a 5 años', href:''},
    {type: 'a', content: 'Comisiones', href:''},
    {type: 'a', content: 'Indicador Morningstar', href:''},
    {type: 'a', content: 'Indicador +AFORE', href:''}
]