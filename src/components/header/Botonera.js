import React from "react";

import cuentaIcon from "../../assets/img/header/cuenta-icon.svg";
import ahorroIcon from "../../assets/img/header/ahorro-icon.svg";
import tramitesIcon from "../../assets/img/header/tramites-icon.svg";
import retirosIcon from "../../assets/img/header/retiros-icon.svg";
// Styles
import './Header.scss';

const Botonera = () => {
  return (
    <div className="p-d-flex p-flex-column p-flex-md-row botonera p-ai-center">
      <div className="p-mb-2 p-col p-d-flex p-jc-center p-ai-center">
        <img
        src={cuentaIcon}
        alt="Cuenta"
        height="50px"
        className="p-mr-3"
        />
        Mi Cuenta
      </div>
      <div className="p-mb-2 p-col p-d-flex p-jc-center p-ai-center">
        <img
          src={ahorroIcon}
          alt="Ahorro"
          height="50px"
          className="p-mr-3"
        />
        Mi Ahorro
      </div>
      <div className="p-mb-2 p-col p-d-flex p-jc-center p-ai-center">
        <img
          src={tramitesIcon}
          alt="Tramites"
          height="50px"
          className="p-mr-3"
        />
        Mis Trámites
      </div>
      <div className="p-mb-2 p-col p-d-flex p-jc-center p-ai-center">
        <img
          src={retirosIcon}
          alt="Retiros"
          height="50px"
          className="p-mr-3"
        />
        Mis Retiros
      </div>
    </div>
  );
}

export default Botonera;
