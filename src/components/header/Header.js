import React, { useState } from "react";
import { Link, withRouter, Redirect } from "react-router-dom";
import aforewebImg from "../../assets/img/header/logo-afore.png";
import ayudaIcon from "../../assets/img/header/ayuda-icon.svg";
import Botonera from "./Botonera";

import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";

import MainMenuContent from "./MainMenuContent";
// Styles
import "./Header.scss";

const Header = () => {
  const [menu, setMenu] = useState(null);
  const [eChild, setEChild] = useState(null);
  const [logged, setLogged] = useState(true);
  const openMainMenuContent = (e, modal) => {
    setEChild(e);
    setMenu(modal);
  };

  const closeAllModals = () => {
    setEChild(null);
    setMenu(null);
  };

  if (true) {
    return (
      <>
        <MainMenuContent
          eChild={eChild}
          isOpen={menu !== null}
          typeMenu={menu}
          clear={closeAllModals}
        />
        <div className="p-grid p-d-flex p-flex-column p-flex-md-row header">
          <div className="p-col-6 p-d-flex p-jc-left p-ai-center">
            <img
              src={aforewebImg}
              alt="aforeWeb"
              height="100px"
              className="p-ml-6"
            />
            {logged && (
              <div className="user-name">
                Hola <span className="user-bold">Ricardo Salgado</span>{" "}
                !Bienvedido
              </div>
            )}
          </div>
          <div className="p-col-6 p-d-flex  p-ai-center p-justify-center">
            <Button
              label="Iniciar sesión"
              className="bttn-sesion p-button-rounded p-py-1"
            />
            <Button
              label="Registrarme"
              className="bttn-registro p-button-rounded p-ml-2 p-py-1"
            />
            <Link
              className="p-d-flex p-ml-4 ayuda"
              onClick={e => openMainMenuContent(e, "h")}
            >
              <img
                src={ayudaIcon}
                alt="ayuda"
                className="p-ai-center hlp-icon"
              />
              <span className="p-ml-2">Ayuda</span>
            </Link>
            <span className="p-input-icon-right p-ml-6">
              <i className="pi pi-search" />
              <InputText placeholder="Buscar" className="p-py-1" />
            </span>
            <span className="p-ml-4 bars p-ml-5">
              <Link>
                <i
                  className="pi pi-bars bars-color"
                  onClick={e => openMainMenuContent(e, "m")}
                />
              </Link>
            </span>
          </div>
        </div>
        <Botonera />
      </>
    );
  }
  return <Redirect to="/login" />;
};

export default withRouter(Header);
