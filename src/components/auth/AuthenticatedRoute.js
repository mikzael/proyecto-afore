import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import AuthenticationService from '../../service/AuthenticationService';

const AuthenticatedRoute = (props) => {
    const isAuth = () =>{
        if (AuthenticationService.isUserLoggedIn()) {
            return <Route {...props} />
        } else {
            return <Redirect to="/arquitectura/login" />
        }
    }

    return(
        isAuth()
    )
}

export default AuthenticatedRoute