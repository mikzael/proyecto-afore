# Afore Web

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# Secure Service

## Custom Environment Variables
Follow the next rules to add variables to `.env`:
- Apart from a few built-in variables (NODE_ENV and PUBLIC_URL), variable names must start with REACT_APP_ to work.
- The environment variables are injected at build time. If you need to inject them at runtime, <a href="https://create-react-app.dev/docs/title-and-meta-tags#generating-dynamic-meta-tags-on-the-server">follow this approach instead.</a> 


For more information visit  <a href="https://create-react-app.dev/docs/adding-custom-environment-variables/">Adding Custom Environment Variables</a>

### What other .env files can be used?
- `.env`: Default. (Not Recomendable, use specific enviroments).
- `.env.development`, `.env.test`, `.env.production`: Environment-specific settings.

